<?php
namespace console\controllers;

use common\rbac\ManagerRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        // add "viewManufacturer" permission
        $viewManufacturer = $auth->createPermission('viewManufacturer');
        $viewManufacturer->description = 'View data about manufacturers';
        $auth->add($viewManufacturer);

        // add "addCategory" permission
        $addCategory = $auth->createPermission('addCategory');
        $addCategory->description = 'Add in category';
        $auth->add($addCategory);

        // add "updateCategory" permission
        $updateCategory = $auth->createPermission('updateCategory');
        $updateCategory->description = 'Change category';
        $auth->add($updateCategory);

        // add "addAndUpdateProduct" permission
        $addAndUpdateProduct = $auth->createPermission('addAndUpdateProduct');
        $addAndUpdateProduct->description = 'Add and update product';
        $auth->add($addAndUpdateProduct);

        // add "addAndUpdateManufacturer" permission
        $addAndUpdateManufacturer = $auth->createPermission('addAndUpdateManufacturer');
        $addAndUpdateManufacturer->description = 'Add and update manufacturer';
        $auth->add($addAndUpdateManufacturer);

        // add "delete" permission
        $delete = $auth->createPermission('delete');
        $delete->description = 'Add and update manufacturer';
        $auth->add($delete);

        // add "authUser" role
        $authUser = $auth->createRole('authUser');
        $auth->add($authUser);
        $auth->addChild($authUser, $viewManufacturer);

        // add "manager" role
        $manager = $auth->createRole('manager');
        $auth->add($manager);
        $auth->addChild($manager, $addCategory);
        $auth->addChild($manager, $authUser);
        /*$auth->addChild($manager, $updateCategory);
        $auth->addChild($manager, $addAndUpdateProduct);*/

        // add "admin" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updateCategory);
        $auth->addChild($admin, $addAndUpdateProduct);
        $auth->addChild($admin, $addAndUpdateManufacturer);
        $auth->addChild($admin, $manager);

        // add "superAdmin" role
        $superAdmin = $auth->createRole('superAdmin');
        $auth->add($superAdmin);
        $auth->addChild($superAdmin, $delete);
        $auth->addChild($superAdmin, $admin);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($superAdmin, 2);
        $auth->assign($manager, 4);
         $auth->assign($admin, 3);
        $auth->assign($authUser, 5);
    }

    public function actionCreateRule(){
        $auth = Yii::$app->authManager;

        // add the rule
        $rule = new ManagerRule();
        $auth->add($rule);

        // add the "updateOwnCategory" permission and associate the rule with it.
        $updateOwnCategory = $auth->createPermission('updateOwnCategory');
        $updateOwnCategory->description = 'Update own category';
        $updateOwnCategory->ruleName = $rule->name;
        $auth->add($updateOwnCategory);

// "updateOwnPost" will be used from "updatePost"
        $updateCategory = $auth->getPermission('updateCategory');
        $auth->addChild($updateOwnCategory, $updateCategory);

// allow "author" to update their own posts
        $manager = $auth->getRole('manager');
        $auth->addChild($manager, $updateOwnCategory);
    }
}