<?php


namespace app\models;


use yii\base\Model;

class Image extends Model
{
    public static function loadImage($image, $extension){
        if( $extension == 'jpg' ) {
            return imagecreatefromjpeg($image);
        } elseif( $extension == 'png' ) {
            return imagecreatefrompng($image);
        }
    }

    public static function getWidth($image){
        return imagesx($image);
    }

    public static function getHeight($image) {
        return imagesy($image);
    }

    public static function save($image, $filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
        if( $image_type == IMAGETYPE_JPEG ) {
            imagejpeg($image,$filename,$compression);
        } elseif( $image_type == IMAGETYPE_PNG ) {
            imagepng($image,$filename);
        }
        if( $permissions != null) {
            chmod($filename,$permissions);
        }
    }

    public static function resize($width, $height, $image) {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $image, 0, 0, 0, 0, $width, $height, Image::getWidth($image), Image::getHeight($image));
        return $new_image;
    }
}