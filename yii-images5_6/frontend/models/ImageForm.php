<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $newImageName;
    public $oldImageName;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
        ];
    }

    public function setNewImageName()
    {
        $this->newImageName = Yii::$app->security->generateRandomString() . '.' . $this->imageFile->extension;
    }

    public function getFolder($directory)
    {
        return Yii::getAlias('@frontend') .  '/web/images/' . $directory . '/';
    }

    public function upload($directory)
    {
        //echo $this->imageFile->tempName;
        if ($this->validate()) {
            $image = Image::loadImage($this->imageFile->tempName, $this->imageFile->extension);
            $width = Image::getWidth($image);
            $height = Image::getHeight($image);

            if($width / $height == 0.75)
                $image = Image::resize(900, 700, $image);
            elseif ($height / $width == 0.75)
                $image = Image::resize(700, 900, $image);

            Image::save($image,$this->getFolder($directory) . $this->newImageName);
            return true;
        } else {
            return false;
        }
    }
}