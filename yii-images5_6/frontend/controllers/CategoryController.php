<?php

namespace frontend\controllers;

use app\models\FileSystem;
use app\models\ImageForm;
use Yii;
use app\models\Category;
use app\models\CategorySearch;
use app\models\CategoryProduct;

use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\models\Product;
use yii\models\ProductSearch;



/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
        'access' => [
            'class' => AccessControl::className(),
            'only' => ['create', 'update', 'delete'],
            'rules' => [
                [
                    'actions' => ['create', 'update', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Category models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search([$searchModel->formName()=>['parent_id'=>0]]);

        return $this->render('newIndex', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Category model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $searchModel = new CategorySearch();
        $dataProvider = $searchModel->search([$searchModel->formName()=>['parent_id'=>$model->category_id]]);
        $children = $dataProvider;
        $stack = [];
        $count = 0;

        while($model->parent_id != 0){
            $stack[] = $this->findModel($model->parent_id);
            $model = $this->findModel($model->parent_id);
            $count++;
        }
        $stack['count'] = $count;
        $model = $this->findModel($id);
        return $this->render('newView', [
            'model' => $model,
            'children' => $children,
            'stack' => $stack,
        ]);
    }

    /**
     * Creates a new Category model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if(!Yii::$app->user->can('addCategory')){
            throw new ForbiddenHttpException('No access');
        }
        $model = new Category();
        $model->parent_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->category_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Category model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if(!Yii::$app->user->can('updateCategory', ['post' => $model])){
            throw new ForbiddenHttpException('No access');
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->category_id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Category model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if(!Yii::$app->user->can('delete')){
            throw new ForbiddenHttpException('No access');
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Category model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Category the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Category::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionImageform($id = 0)
    {
        $model = new ImageForm();
        $product = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $model->setNewImageName();
            if ($model->upload('category')) {
                // file is uploaded successfully
                $model->oldImageName = $product->image;
                $product->image = $model->newImageName;

                if($product->save()){
                    FileSystem::deleteFile($model->getFolder('category') . $model->oldImageName);
                    return $this->redirect(['view', 'id' => $id]);
                } else{
                    FileSystem::deleteFile($model->getFolder('category') . $model->newImageName);
                }
            }
        }

        return $this->render('uploadForm', ['model' => $model]);
    }
}
