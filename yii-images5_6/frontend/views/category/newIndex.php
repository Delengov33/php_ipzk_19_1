<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $model app\models\Category */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories'];
\yii\web\YiiAsset::register($this);
?>
<div class="category-view">


    <?php
    if($dataProvider->count != 0) :
        ?>
        <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_viewItem',
        'layout' => "{items}",

        'itemOptions' => [
            'tag' => false,
        ]
    ])
        ?>
    <?php
    endif;
    ?>
</div>
