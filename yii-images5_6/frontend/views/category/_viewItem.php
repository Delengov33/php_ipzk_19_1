<?php

use yii\helpers\Url;
use yii\helpers\Html;

?>
<div class="col-sm-4 col-md-4">
    <a href="<?= Url::to(['category/view', 'id' => $model['category_id']]);?>">
        <?= Html::encode($model->name) ?>
    </a> <br>

        <img src="<?= Html::encode($model->getImage())?>" style="max-height: 300px; max-width: 300px"/><br>
        <?= Html::encode($model->description) ?>
</div>
