<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
\yii\web\YiiAsset::register($this);



$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
for($i = $stack['count'] - 1; $i >= 0; $i--){
    $this->params['breadcrumbs'][] = ['label' => $stack[$i]->name, 'url' => ['view', 'id' => $stack[$i]->category_id]];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $model->description ?>







    </p>
    <?php
    if($children->count != 0) :
    ?>
    <?= ListView::widget([
        'dataProvider' => $children,
        'itemView' => '_viewItem',
        'layout' => "{items}",

        'itemOptions' => [
            'tag' => false,
        ]
    ])
    ?>
    <?php
    endif;
    ?>
</div>
