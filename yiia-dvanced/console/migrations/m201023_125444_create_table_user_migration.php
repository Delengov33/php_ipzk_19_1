<?php

use yii\db\Migration;

/**
 * Class m201023_125444_create_table_user_migration
 */
class m201023_125444_create_table_user_migration extends Migration
{
   public function up()
      {
          $this->createTable('user', [
                     'id' => $this->primaryKey(),
                     'name' => $this->string(255),
                     'password' => $this->text(),

                 ]);
   $this->createTable('migration', [
                     'id' => $this->primaryKey(),
                     'name' => $this->string(255),
                     'location' => $this->text(),

                 ]);

          $this->addColumn('{{%user}}', 'verification_token', $this->string()->defaultValue(null));
      }

      public function down()
      {
      $this->dropTable('user');
      $this->dropTable('migration');

          $this->dropColumn('{{%user}}', 'verification_token');

      }
    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201023_125444_create_table_user_migration cannot be reverted.\n";

        return false;
    }
    */
}
