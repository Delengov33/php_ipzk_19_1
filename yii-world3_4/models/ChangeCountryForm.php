<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Country;

/**
 * ContactForm is the model behind the contact form.
 */
class ChangeCountryForm extends Model
{
    public $name;
    public $capital;
    public $area;
    public $currency;
    public $continent;
    public $continentsCodes;
    public $id;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'capital', 'area', 'currency', 'continent'], 'required'],
            // email has to be a valid email address
            // ['email', 'email'],
            // verifyCode needs to be entered correctly
            // ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
//    public function attributeLabels()
//    {
//        return [
//            'verifyCode' => 'Verification Code',
//        ];
//    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */



    public function changeCountry()
    {
        if ($this->validate()) {
            $continentCodes = array_map(function($value) {return $value->code;}, $this->continentsCodes);
            $continentId = array_search($this->continent ,$continentCodes) + 1;

            $country = Country::findOne($this->id);
            $country->name = $this->name;
            $country->capital = $this->capital;
            $country->area = $this->area;
            $country->currency = $this->currency;
            $country->continent_id = $continentId;
            $country->update();

//            Country::updateAll(['name' => $this->name,
//                'capital' => $this->capital,
//                'area' => $this->area,
//                'currency' => $this->currency,
//                'continent_id' => $continentId,], ['country_id', $this->id]);
            return true;
        }
        return false;
    }
}
