<?php


namespace app\controllers;

use app\models\CityLanguage;
use app\models\Continent;
use app\models\RegionLanguage;
use yii\web\Controller;
use app\models\WorldForm;
use app\models\Country;
use Yii;

class PjaxController extends Controller
{
    public function actionWorldForm()
    {
        $model = new WorldForm();
        $continents = Continent::find()->asArray()->all();
        $countries = null;
        $continent = null;
        $country = null;
        $regions = null;
        $region = null;
        $cities = null;
        $city = null;
        $data = null;
        $key = '39688af7d3b663837d2509a4bad795c7';


        if (Yii::$app->request->isPjax) {
            $model->load(Yii::$app->request->post());
            $continent = Continent::find()->where(['continent_id' => $model->continent_id])->one();
            $countries = Country::find()->where(['continent_id' => $model->continent_id])->asArray()->all();
            $country = Country::find()->where(['country_id' => $model->country_id])->one();
            $regions = RegionLanguage::find()->join('inner join', 'region', 'region_language.region_id = region.region_id')->where(['language' => 'ua', 'country_id' => $model->country_id])->asArray()->all();
            $region = RegionLanguage::find()->join('inner join', 'region', 'region_language.region_id = region.region_id')->where(['language' => 'ua', 'region.region_id' => $model->region_id])->one();
            $cities = CityLanguage::find()->join('inner join', 'city', 'city_language.city_id = city.city_id')->where(['language' => 'ua', 'region_id' => $model->region_id])->asArray()->all();
            $city = CityLanguage::find()->join('inner join', 'city', 'city_language.city_id = city.city_id')->where(['language' => 'ua', 'city.city_id' => $model->city_id])->one();
            $url = "api.openweathermap.org/data/2.5/weather?q={$city->name_language}&lang=ua&units=metric&appid={$key}";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $url);
            $data = json_decode(curl_exec($ch));
            curl_close($ch);
        }
        return $this->render('world-form', [
            'model' => $model,
            'continents' => $continents,
            'continent' => $continent,
            'countries' => $countries,
            'country' => $country,
            'regions' => $regions,
            'region' => $region,
            'cities' => $cities,
            'city' => $city,
            'data' => $data
        ]);
    }
}