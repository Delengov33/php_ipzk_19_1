<?php

namespace app\controllers;

use Yii;
use app\models\RegionLanguage;
use app\models\RegionLanguageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RegionLanguageController implements the CRUD actions for RegionLanguage model.
 */
class RegionLanguageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RegionLanguage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RegionLanguageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RegionLanguage model.
     * @param integer $region_id
     * @param string $language
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($region_id, $language)
    {
        return $this->render('view', [
            'model' => $this->findModel($region_id, $language),
        ]);
    }

    /**
     * Creates a new RegionLanguage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RegionLanguage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'region_id' => $model->region_id, 'language' => $model->language]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RegionLanguage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $region_id
     * @param string $language
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($region_id, $language)
    {
        $model = $this->findModel($region_id, $language);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'region_id' => $model->region_id, 'language' => $model->language]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RegionLanguage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $region_id
     * @param string $language
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($region_id, $language)
    {
        $this->findModel($region_id, $language)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RegionLanguage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $region_id
     * @param string $language
     * @return RegionLanguage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($region_id, $language)
    {
        if (($model = RegionLanguage::findOne(['region_id' => $region_id, 'language' => $language])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
