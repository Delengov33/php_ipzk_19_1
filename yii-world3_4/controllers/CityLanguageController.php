<?php

namespace app\controllers;

use Yii;
use app\models\CityLanguage;
use app\models\CityLanguageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CityLanguageController implements the CRUD actions for CityLanguage model.
 */
class CityLanguageController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CityLanguage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CityLanguageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CityLanguage model.
     * @param integer $city_id
     * @param string $language
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($city_id, $language)
    {
        return $this->render('view', [
            'model' => $this->findModel($city_id, $language),
        ]);
    }

    /**
     * Creates a new CityLanguage model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CityLanguage();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'city_id' => $model->city_id, 'language' => $model->language]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CityLanguage model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $city_id
     * @param string $language
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($city_id, $language)
    {
        $model = $this->findModel($city_id, $language);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'city_id' => $model->city_id, 'language' => $model->language]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CityLanguage model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $city_id
     * @param string $language
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($city_id, $language)
    {
        $this->findModel($city_id, $language)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CityLanguage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $city_id
     * @param string $language
     * @return CityLanguage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($city_id, $language)
    {
        if (($model = CityLanguage::findOne(['city_id' => $city_id, 'language' => $language])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
