<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
?>

<?php Pjax::begin([
    'id' => 'world-form-container'
]); ?>
<?php $form = ActiveForm::begin([
    'options' => ['data' => ['pjax' => true],],
    'id' => 'world-form'
]); ?>
 <div class="row">
  <div class="col-xs-12 col-sm-7 ">
<?= $form->field($model, 'continent_id')->dropDownList(ArrayHelper::map($continents, 'continent_id', 'name'), [
    'id' => 'field-continent-id',
    'onchange' => '
     $("#field-country-id").val(0);
                        $("#field-region-id").val(0);
                        $("#field-city-id").val(0);
    $("#world-form").submit()'
]) ?>


<?php if ($countries != null) :?>
<?= $form->field($model, 'country_id')->dropDownList(ArrayHelper::map($countries, 'country_id', 'name'), [
    'id' => 'field-country-id',
    'onchange' => '$("#world-form").submit()'
]) ?>

<?php endif; ?>
<?php if ($regions != null) :?>
    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($regions, 'region_id', 'name_language'), [
        'id' => 'field-region-id',
        'onchange' => '$("#world-form").submit()'
    ]) ?>

<?php endif; ?>
<?php if ($cities != null) :?>
    <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map($cities, 'city_id', 'name_language'), [
        'id' => 'field-city-id',
        'onchange' => '$("#world-form").submit()'
    ]) ?>



<?php endif; ?>
 </div>
     <div class="col-xs-12 col-sm-5 ">
   <h2><?= $continent->name ?></h2>
      <p><?= $continent->description ?></p>
   <h2><?= $country->name ?></h2>
      <p><?= $country->code ?></p>
       <h2><?= $region->name_language ?></h2>
    </div>
    </div>
    <?php if (isset($data->name)) :?>
   <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-3 label-info" style="height:100px"><h2>Погода в місті <?= $city->name_language ?></h2></div>
<div class="col-xs-12 col-sm-3  label-info" style="height:100px"><p>Температура: <?=$data->main->temp?></p></div>
<div class="col-xs-12 col-sm-3 label-info" style="height:100px"><p>Вологість: <?=$data->main->humidity?></p></div>
<div class="col-xs-12 col-sm-3 label-info" style="height:100px"><p>Тиск: <?=$data->main->pressure?></p></div></div>
<?php endif; ?>
<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>