<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CityLanguage */

$this->title = 'Update City Language: ' . $model->city_id;
$this->params['breadcrumbs'][] = ['label' => 'City Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->city_id, 'url' => ['view', 'city_id' => $model->city_id, 'language' => $model->language]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="city-language-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
