<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CityLanguage */

$this->title = 'Create City Language';
$this->params['breadcrumbs'][] = ['label' => 'City Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-language-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
