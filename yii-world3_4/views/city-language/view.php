<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CityLanguage */

$this->title = $model->city_id;
$this->params['breadcrumbs'][] = ['label' => 'City Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="city-language-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'city_id' => $model->city_id, 'language' => $model->language], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'city_id' => $model->city_id, 'language' => $model->language], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'city_id',
            'language',
            'name_language',
        ],
    ]) ?>

</div>
