<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegionLanguage */

$this->title = 'Update Region Language: ' . $model->region_id;
$this->params['breadcrumbs'][] = ['label' => 'Region Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->region_id, 'url' => ['view', 'region_id' => $model->region_id, 'language' => $model->language]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="region-language-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
