<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RegionLanguage */

$this->title = 'Create Region Language';
$this->params['breadcrumbs'][] = ['label' => 'Region Languages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-language-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
