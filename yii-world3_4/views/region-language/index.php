<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RegionLanguageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Region Languages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="region-language-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Region Language', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'region_id',
            'language',
            'name_language',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
