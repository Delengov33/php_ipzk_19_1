<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Country */

$this->title = $country->name;
$this->params['breadcrumbs'][] = ['label' => $country->continent->name, 'url' => ['continent/view','code'=>$country->continent->code]];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="country-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $country->country_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $country->country_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<div class="row">
<div class="col-md-6 col-xs-12">
    <?= DetailView::widget([
        'model' => $country,
        'attributes' => [
            'country_id',
            'code',
            'name',
            'official_name',
            ['attribute'=>'Flag',
            'value'=>"@web/images/countries/png100px/".strtolower( $country->code).'.png',
            'format'=>['image',['width'=>'100']]

            ],

            'number',
            'currency',
            'capital',
            'area',
            'continent_id',
            'coords',
            'display_order',
        ],
    ]) ?>
    </div>
    <div class="col-md-6 col-xs-12 "  >

          <div id="map" date-coords="<?=$country->coords?>" style="width:100%; height:300px; margin-left:auto; margin-right:auto"></div>
</div>
</div>
<script>
        function initMap() {
          // The location of Uluru
let la=document.querySelector('#map').getAttribute('date-coords').toString();

                   la=la.split(',');
          const uluru = { lat: parseFloat(la[0].slice(1)),lng:parseFloat(la[1].slice(0,-1))  };
console.log(uluru);
          // The map, centered at Uluru
          const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 4,
            center: uluru,
          });


          // The marker, positioned at Uluru
          const marker = new google.maps.Marker({
            position: uluru,
            map: map,
          });
        }
</script>
<script
      src="https://maps.googleapis.com/maps/api/js?callback=initMap&sensor=false"
      defer
    ></script>


</div>

