<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\WorldForm */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
?>

<?php Pjax::begin([
    'id' => 'world-form-container'
]); ?>
<div class="row">
<?php $form = ActiveForm::begin([
    'options' => ['data' => ['pjax' => true], 'class' => 'col-6 col-lg-6 col-sm-6',],
    'id' => 'world-form',
]); ?>

<?= $form->field($model, 'continent_id')->dropDownList(ArrayHelper::map($continents, 'continent_id', 'name'), [
    'prompt' => 'Continent',
    'id' => 'field-continent-id',
    'onchange' => ' 
                    $("#field-country-id").val(0);
                    $("#field-region-id").val(0);
                    $("#field-city-id").val(0);
                    $("#world-form").submit()'
]) ?>

<?php
if($countries) :
?>
<?= $form->field($model, 'country_id')->dropDownList(ArrayHelper::map($countries, 'country_id', 'name'), [
    'prompt' => 'Country',
    'id' => 'field-country-id',
    'onchange' => ' $("#field-region-id").val(0);
                    $("#field-city-id").val(0);
                    $("#world-form").submit()'
]) ?>
<?php
    endif;
?>

<?php
if($regions) :
    ?>
    <?= $form->field($model, 'region_id')->dropDownList(ArrayHelper::map($regions, 'region_id', 'name_language'), [
    'prompt' => 'Region',
    'id' => 'field-region-id',
    'onchange' => ' $("#field-city-id").val(0);
                    $("#world-form").submit()'
]) ?>
<?php
endif;
?>

<?php
if($cities) :
    ?>
    <?= $form->field($model, 'city_id')->dropDownList(ArrayHelper::map($cities, 'city_id', 'name_language'), [
    'prompt' => 'City',
    'id' => 'field-city-id',
    'onchange' => '$("#world-form").submit()'
]) ?>
<?php
endif;
if($weather) :?>
    <?= Html::encode(  'Температура: ' . $weather->main->temp);?><br>
    <?= Html::encode(  'Вологість: ' . $weather->main->humidity);?><br>
    <?= Html::encode(  'Тиск: ' . $weather->main->pressure);?><br>
    <?= Html::img(' http://openweathermap.org/img/wn/' . $weather->weather[0]->icon . '.png',
        ['alt' => 'хмарність', 'class' => 'img-thumbnail'])?>
<?php
//print_r($weather->weather[0]->icon);
endif;
?>

<?php ActiveForm::end(); ?>
    <table class="col-6 col-lg-6 col-sm-6 table-bordered table-striped">
        <tr><td>Continent</td><td><?= $continent->name ?></td></tr>
        <tr><td>Country</td><td><?= $country->name ?></td></tr>
        <tr><td>Region</td><td><?= $region['name_language'] ?></td></tr>
        <tr><td>City</td><td><?= $city['name_language'] ?></td></tr>
    </table>
<?php Pjax::end(); ?>
</div>
