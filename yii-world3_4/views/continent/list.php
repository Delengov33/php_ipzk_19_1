<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="container">
    <div class="row justify-content-between">



<?php foreach($continents as $continentsItem):?>
<div class="col-md-4 col-xs-12 " style="min-height: 500px">
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">
    <a href="<?=Url::to(['continent/view', 'code'=>$continentsItem['code']]);?>">
    <?php echo $continentsItem['name'];?>
    </a>
    </h3>
  </div>
  <div class="panel-body">
  <?=Html::img('@web/images/continents/'.$continentsItem['code'] .'.png',['width'=>'100%']);?>
   <?php echo $continentsItem['description'];?>
  </div>
</div>
</div>
<?php endforeach; ?>

    </div>
</div>
